package application;
	
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class Main extends Application {
	
	@FXML
	private javafx.scene.control.TextField consoleOutput; // name should be same as the fx:id
	@Override
	public void start(Stage primaryStage) {
		
		try {
			
			AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("Applitools_PDF.fxml"));
			
			Scene scene = new Scene(root,600,650);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.initStyle(StageStyle.UTILITY);
			primaryStage.setResizable(false);
			primaryStage.setTitle("Applitools - PDF Helper");
			primaryStage.show();
			
		} catch(Exception e) {
			
			e.printStackTrace();
			
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
