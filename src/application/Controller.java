package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller {

	@FXML
	private Button myButton; // name should be same as the fx:id
	
	@FXML
	private TextField apiKey; // name should be same as the fx:id
	
	@FXML
	private TextField pdfLocation; // name should be same as the fx:id
	
	@FXML
	private javafx.scene.control.TextField consoleOutput; // name should be same as the fx:id

	@FXML
	public void myAction(ActionEvent event) throws Exception {
		
		String apiKeyString = apiKey.getText().toString();
		String location = pdfLocation.getText().toString();
		
		System.out.println(location.replace("\\", "\\\\"));
		
	 ExecuteBatchCommandClass.executeCommand(apiKeyString,location);
		consoleOutput.setVisible(true);
		
		consoleOutput.setText("Done Uploading... Running the Tests in Applitools...");

	}

	

}
